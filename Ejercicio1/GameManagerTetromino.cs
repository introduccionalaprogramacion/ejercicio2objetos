﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerTetromino : MonoBehaviour
{

   
    string[] type = {"straight","square", "L-tetromino", "T-tetromino", "Z-tetromino" };
    string[] color = {"rojo","azul","verde","rosa", "amarillo" };
    List<Tetromino> tetris = new List<Tetromino>();

    // Start is called before the first frame update
    void Start()
    {
        for (int i=0; i<10; i++) { 
        tetris.Add(newPieza());
        }

        tetris[2].rotateTetromino();
        tetris[3].rotateTetromino(270);
        tetris[6].increaseVelocity();
        tetris[8].decreaseVelocity();

        for (int i=0; i<tetris.Count; i++) {
            Debug.Log(tetris[i].showInfo());
        }
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    Tetromino newPieza() {
        int posicionRandom = Random.Range(0, type.Length);
        return new Tetromino(type[posicionRandom],color[posicionRandom]);
    }
}
