﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetromino : MonoBehaviour
{
   public string colour;
    public int velocity = 0;
    public int rotation = 0;
    public string type;


    // Start is called before the first frame update
   void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


   public int increaseVelocity() {
        return this.velocity += 10;
    }

    public int decreaseVelocity() {
        return this.velocity -= 10;
    }
    public int rotateTetromino() {
        return this.rotation += 90;
    }

    public int rotateTetromino(int rotacion) {       
        switch (rotacion) {
            case 0:               
            case 90:              
            case 180:                
            case 270:             
                return this.rotation = rotacion; break;
            default:
                return this.rotation = -1; break; 
        }       
    }
    public bool isMoving() {
        if (this.velocity > 0) return true;
        else return false;
    }
    public Tetromino(string tipo, string color) {
        this.type = tipo;
        this.colour = color;
    }

    public Tetromino() {   
        
    }
    public string showInfo() {
        return "El Tetromino: " + this.type + " con el color: " + this.colour + ", va a una velocidad de " + this.velocity + " y esta rotado "  +this.rotation;
    }
}
