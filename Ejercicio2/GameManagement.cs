﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagement : MonoBehaviour {
    List<Card> cartasJuego;
    // Start is called before the first frame update
    void Start()
    {
         cartasJuego  = initDeck();        
        Player player1 = new Player();
        for (int i=0;i<5;i++) {
            player1.cartasJugador = player1.dealCard(nextCard());
        }
        
            
        Debug.Log("Has obtenido: "+player1.getPoints()+" puntos.");
        Debug.Log("Con las siguientes cartas: ");
       
            for (int i = 0; i < player1.cartasJugador.Count; i++) {
                player1.cartasJugador[i].printInfo();
            }
       
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    List<Card>initDeck()
    {
        List<Card> cartas = new List<Card>();
        List<Card> cartaBarajada = new List<Card>();
        cartas.Add(new Card("Espadas", "As", 10));
        string nombre="";
        for (int i = 2; i <= 12; i++)
        {
            if (i >= 10)
            {
                switch (i) {
                    case 10: nombre = "10";break;
                    case 11: nombre = "J";break;
                    case 12: nombre = "Q";break;
                    case 13: nombre = "K";break;

                }
                cartas.Add(new Card("Espadas", nombre, 10));
            }
            else
            {
                cartas.Add(new Card("Espadas", i.ToString(), i));
            }
        }
        cartas.Add(new Card("Rombos", "As", 10));
        for (int i = 2; i <= 12; i++)
        {
            if (i >= 10)
            {
                switch (i)
                {
                    case 10: nombre = "10"; break;
                    case 11: nombre = "J"; break;
                    case 12: nombre = "Q"; break;
                    case 13: nombre = "K"; break;

                }
                cartas.Add(new Card("Rombos", nombre, 10));
            }
            else
            {
                cartas.Add(new Card("Rombos", i.ToString(), i));
            }
        }
        cartas.Add(new Card("Corazones", "As", 10));
        for (int i = 2; i <= 12; i++)
        {
                if (i >= 10)
                {
                    switch (i)
                    {
                        case 10: nombre = "10"; break;
                        case 11: nombre = "J"; break;
                        case 12: nombre = "Q"; break;
                        case 13: nombre = "K"; break;

                    }
                    cartas.Add(new Card("Corazones", nombre, 10));
                }
                else
                {
                    cartas.Add(new Card("Corazones", i.ToString(), i));
                }
        }
        cartas.Add(new Card("Tréboles", "As", 10));
        for (int i = 2; i <= 12; i++)
        {
            if (i >= 10)
            {
                switch (i)
                {
                    case 10: nombre = "10"; break;
                    case 11: nombre = "J"; break;
                    case 12: nombre = "Q"; break;
                    case 13: nombre = "K"; break;

                }
                
                cartas.Add(new Card("Tréboles", nombre, 10));
            }
            else
            {
                cartas.Add(new Card("Tréboles", i.ToString(), i));
            }
        }
        for (int i = cartas.Count; i > 0; i--)
        {
            int nRandom = Random.Range(0, cartas.Count);
            Card newCard = cartas[nRandom];
            cartaBarajada.Add(newCard);
            cartas.Remove(newCard);
        }

        return cartaBarajada;

    }

    Card nextCard() {
        try
        {
            Card cartaRepartir = this.cartasJuego[Random.Range(0, this.cartasJuego.Count)];
            this.cartasJuego.Remove(cartaRepartir);
            return cartaRepartir;
        }
        catch (System.IndexOutOfRangeException oute) {
            Debug.Log(oute+". Something went wrong, try again", this);
            return null;

        }
     }


}
