﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card
{
    public string palo;
    public string numero;
    public int puntos;       
    
    public void printInfo() {
      Debug.Log(this.numero+" de "+ this.palo+ ", otorga: "+this.puntos+" puntos.");
    }

    public Card(string name, string number, int points)
    {
        this.palo = name;
        this.numero = number;
        this.puntos = points;
    }
}
  

