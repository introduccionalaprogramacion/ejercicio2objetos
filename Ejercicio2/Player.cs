﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player{

    public List<Card> cartasJugador= new List<Card>();
    int puntuacion=0;

    public List<Card> dealCard(Card carta) {
        this.cartasJugador.Add(carta);
        setPoints(carta);
        return this.cartasJugador;
    }
    
    private int setPoints(Card carta) {     
        this.puntuacion += carta.puntos;        
        return this.puntuacion;
    }


  public int getPoints() {
        return this.puntuacion;
    }
}
