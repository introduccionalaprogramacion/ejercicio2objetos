﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    //atributes
    public string name;
    public int positionX;
    public int positionY;
    public int hp;   
    //Constructor vacio
    public Enemigo() { }
    //constructor con param
    public Enemigo(string nombre, int posicionX, int posicionY, int vida)
    {
        name = nombre;
        positionX = posicionX;
        positionY = posicionY;
        this.hp =vida;
    }
    string toString()
    {
        return "El enemigo " + name + " está en la posición: " + positionX + "," + positionY + " y tiene: " + hp + " puntos de vida.";
    }
}
